/**
 * Module helping for choosing desired baudrate used in UART.
 *
 * @param clock             - clock used for the output change
 * @param reset             - reset state machine to first state (9600 bauds)
 * @param next              - button input for picking next state
 * @param prescale_value    - choosed prescaler value (using 115200 Hz clock)
 * @param prescale_display  - choosed prescale baudrate for display purposes
 */
module prescaler_state_machine(
    input   wire        clock,
    input   wire        reset,
    input   wire        next,
    
    output  reg[3:0]    prescale_value,
    output  reg[23:0]   prescale_display
);

    reg[3:0]   state;
    localparam state_9600           = 4'd0;
    localparam state_9600_press     = 4'd1;
    localparam state_19200          = 4'd2;
    localparam state_19200_press    = 4'd3;
    localparam state_38400          = 4'd4;
    localparam state_38400_press    = 4'd5;
    localparam state_57600          = 4'd6;
    localparam state_57600_press    = 4'd7;
    localparam state_115200         = 4'd8;
    localparam state_115200_press   = 4'd9;
        
    /* State machine. */
    always @(posedge clock)
    begin
        if (reset)
        begin
            state <= state_9600;
        end
        else
        begin
            case (state)
                state_9600           : state <= (next)  ? state_9600_press      : state_9600;
                state_9600_press     : state <= (!next) ? state_19200           : state_9600_press;
                state_19200          : state <= (next)  ? state_19200_press     : state_19200;
                state_19200_press    : state <= (!next) ? state_38400           : state_19200_press;
                state_38400          : state <= (next)  ? state_38400_press     : state_38400;
                state_38400_press    : state <= (!next) ? state_57600           : state_38400_press;
                state_57600          : state <= (next)  ? state_57600_press     : state_57600;
                state_57600_press    : state <= (!next) ? state_115200          : state_57600_press;
                state_115200         : state <= (next)  ? state_115200_press    : state_115200;
                state_115200_press   : state <= (!next) ? state_9600            : state_115200_press;
            endcase
        end
    end
    
    /* Set prescaler value. */
    always @(posedge clock)
	begin
		case (state)
            state_9600			: prescale_value <= 4'd12;
            state_9600_press	: prescale_value <= 4'd12;
            state_19200			: prescale_value <= 4'd6;
            state_19200_press	: prescale_value <= 4'd6;
            state_38400			: prescale_value <= 4'd3;
            state_38400_press	: prescale_value <= 4'd3;
            state_57600			: prescale_value <= 4'd2;
            state_57600_press	: prescale_value <= 4'd2;
            state_115200		: prescale_value <= 4'd1;
            state_115200_press	: prescale_value <= 4'd1;
		endcase
	end
    
    /* Set display value. */
    always @(posedge clock)
	begin
		case (state)
            state_9600			: prescale_display <= 24'h9600;
            state_9600_press	: prescale_display <= 24'h9600;
            state_19200			: prescale_display <= 24'h19200;
            state_19200_press	: prescale_display <= 24'h19200;
            state_38400			: prescale_display <= 24'h38400;
            state_38400_press	: prescale_display <= 24'h38400;
            state_57600			: prescale_display <= 24'h57600;
            state_57600_press	: prescale_display <= 24'h57600;
            state_115200		: prescale_display <= 24'h115200;
            state_115200_press	: prescale_display <= 24'h115200;
		endcase
	end


endmodule
