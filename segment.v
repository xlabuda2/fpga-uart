/**
 * Module to encode one hexa symbol to the display.
 *
 * @param data          - data to encode
 * @param segment_value - resulting value used for display
 */
module segment(
    input   wire[3:0]   data,
    
    output  reg[6:0]    segment_value
);

always @(data)
begin
    case (data)
        4'h0   : segment_value = 7'b1000000;
        4'h1   : segment_value = 7'b1111001;
        4'h2   : segment_value = 7'b0100100;
        4'h3   : segment_value = 7'b0110000;
        4'h4   : segment_value = 7'b0011001;
        4'h5   : segment_value = 7'b0010010;
        4'h6   : segment_value = 7'b0000010;
        4'h7   : segment_value = 7'b1111000;
        4'h8   : segment_value = 7'b0000000;
        4'h9   : segment_value = 7'b0011000;
        4'hA   : segment_value = 7'b0001000;
        4'hb   : segment_value = 7'b0000011;
        4'hC   : segment_value = 7'b1000110;
        4'hd   : segment_value = 7'b0100001;
        4'hE   : segment_value = 7'b0000110;
        4'hF   : segment_value = 7'b0001110;
        default: segment_value = 7'b1111111; 
    endcase
end

endmodule
