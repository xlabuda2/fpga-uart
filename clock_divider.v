/**
 * Clock divider with inbuilt prescaler.
 *
 * @param clock             - clock input
 * @param reset             - reset current tick
 * @param prescale_value    - avaliable prescaler fur further division
 * @param out               - output of divided clocks
 */
module clock_divider(
    input   wire        clock,
    input   wire        reset,
    input   wire[3:0]   prescale_value,
    
    output  reg         out
);
    
    parameter divider = 32'd50_000_000;

    reg[31:0]   counter;
    reg[3:0]    current_prescale_value;

    always @(posedge clock)
    begin
        if (reset)
        begin
            counter <= counter - 1;
        end
        else if (current_prescale_value > 1)
        begin
            current_prescale_value <= current_prescale_value - 1;
        end
        else
        begin
            current_prescale_value <= prescale_value;
        
            if (!counter)
                counter <= divider;
            else
                counter <= counter - 1;
            
            out <= counter == 0;
         end;
    end

endmodule
