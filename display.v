/**
 * Helping module to join seven segments of the display
 * to single one.
 * 
 * @param data      - data to display
 * @param digits    - encoded data for display
 */
module display(
    input   wire[23:0] data,
    
    output  wire[41:0] digits
);
    
    segment digit0(
        .data(data[3:0]),
        .segment_value(digits[6:0])
    );

    segment digit1(
        .data(data[7:4]),
        .segment_value(digits[13:7])
    );
    
    segment digit2(
        .data(data[11:8]),
        .segment_value(digits[20:14])
    );
    
    segment digit3(
        .data(data[15:12]),
        .segment_value(digits[27:21])
    );
    
    segment digit4(
        .data(data[19:16]),
        .segment_value(digits[34:28])
    );
    
    segment digit5(
        .data(data[23:20]),
        .segment_value(digits[41:35])
    );

endmodule
