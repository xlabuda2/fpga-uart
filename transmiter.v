/**
 * Sends data using UART protocol.
 *
 * @params clock    - clock with same frequency as UART
 * @params start    - on negedge module will start transmiting if it is free,
 *                    if pressed before module is ready, transmission will be paused
 * @params data     - data to transmit
 * @params ready    - holds log.0 when module is bussy
 * @params out      - output of the transmission
 */
module transmiter(
    input   wire        clock,
    input   wire        start,
    input   wire[7:0]   data,
    
    output  reg         ready,
    output  reg         out
);
    
    parameter start_bit = 1'b0;
    parameter stop_bit  = 1'b1;

    /**
     * Current data with start and stop bits and
     * numbers of bits to be transmitted.
     */
    reg [9:0] packet;
    reg [3:0] to_transmit;
    
    /* Transmit data. */
    always @(posedge clock, posedge start)
    begin
        if (start)
        begin
            if (ready)
            begin
                out     <= 1;
                ready   <= 0;
                
                /* Load data with bits to the buffer. */
                to_transmit    <= 10;
                packet         <= (stop_bit << 9) | (data << 1) | start_bit; 
            end
        end
        else if (to_transmit > 0)
        begin
            out <= packet[0];
            
            /* Shift bits in buffer. */
            to_transmit <= to_transmit - 1;
            packet      <= packet >> 1;
        end
        else
        begin
            /* Set ready state. */
            out     <= 1;
            ready   <= 1;
        end
    end 
    
endmodule
