/**
 * Loads data using UART protocol.
 *
 * @param in            - UART link wire
 * @param clock         - clock with 7 times higher frequency then UART
 * @param clear_flag    - log.1 will clear read flag on each clock tick
 * @param out           - recieved data
 * @param ready         - flag signifying whether new data are ready
 */
module receiver(
    input wire      in,
    input wire      clock,
    input wire      clear_flag,
    
    output reg[7:0] out,
    output reg      ready
);

    reg[7:0]    data;
    /* Represents sum of three measurements at single bit. */
    reg[2:0]    current_bit_value;
    /* Represents sum of three measurements at stop bit. */
    reg[2:0]    stop_bit_value;
    /* Represents sum of three measurements at start bit. */
    reg[2:0]    start_bit_value;
    /* Handle previous value from clear_flag param. */
    
    /**
     * Represents which bit is currently read:
     *  0       currently is not reading.
     *  1       reading start bit.
     *  2 - 9   reading data bit.
     *  10      readint stop bit.
     */
    reg[3:0]   state_read;
    localparam state_read_nop = 0;
    localparam state_read_start_bit = 1;
    localparam state_read_first_data_bit = 2;
    localparam state_read_stop_bit = 10;
    localparam state_read_last_bit = 10;

    /**
     * Each bit is computed from three measured values,
     * made in separate time segments durning read:
     *  0 - 1   nothing
     *  2       reading first value
     *  3       reading second value
     *  4       reading third value
     *  5       compute resulting value using weight
     *  6       last segment
     */
    reg[2:0]   state_bit;
    localparam state_bit_first_read = 2;
    localparam state_bit_second_read = 3;
    localparam state_bit_third_read = 4;
    localparam state_bit_calculate = 5;
    localparam state_bit_last_segment = 6;
       
    /**
     * State machine handling current reading state.
     * First readed log.0 wil beggin reading of data.
     */
    always @(posedge clock)
    begin  
        /* Start reading data on first log.0. */
        if (in == 0 && state_read == state_read_nop)
        begin
            state_bit   <= 1;
            state_read  <= 1;
        end
        /* End if read all data. */
        else if (state_read == state_read_last_bit && state_bit == state_bit_last_segment)
        begin
            state_bit   <= 0;
            state_read  <= 0;
        end
        /* Keep reading. */
        else if (state_read != state_read_nop)
        begin
            state_bit <= (state_bit == 6) ? 0 : state_bit + 1;
            state_read <= (state_bit == 6) ? state_read + 1 : state_read;
        end
        
    end
    
    /**
     * Read data according to given states. If read packet was not in the
     * correct format,
     */
    always @(posedge clock)
    begin
        /* Clear ready flag. */
        if (clear_flag)
        begin
            ready <= 0;
        end
    
        /* Idle state. */
        if (state_read == state_read_nop)
        begin
            /* Check if start and stop bit weights ar correct. */
            if (start_bit_value < 2 && stop_bit_value > 1)
            begin
                out     <= data;
                data    <= 0;
                ready   <= 1;
            end
            
            /* Clear previous data. */
            start_bit_value <= 0;
            stop_bit_value <= 0;
        end
        /* Read start bit. */
        if (state_read == state_read_start_bit)
        begin
            /* Load weights. */
            case (state_bit)
                state_bit_first_read:   start_bit_value <= start_bit_value + in;
                state_bit_second_read:  start_bit_value <= start_bit_value + in;
                state_bit_third_read:   start_bit_value <= start_bit_value + in;
            endcase
        end
        /* Read stop bit. */
        if (state_read == state_read_stop_bit) /* Read stop bit */
        begin
            /* Load weights. */
            case (state_bit)
                state_bit_first_read:   stop_bit_value <= stop_bit_value + in;
                state_bit_second_read:  stop_bit_value <= stop_bit_value + in;
                state_bit_third_read:   stop_bit_value <= stop_bit_value + in;
            endcase
        end
        /* Read data bits. */
        if (1 < state_read && state_read < 10) /* Read data bits */
        begin
            case (state_bit)
                /* Load weights. */
                state_bit_first_read:   current_bit_value <= current_bit_value + in;
                state_bit_second_read:  current_bit_value <= current_bit_value + in;
                state_bit_third_read:   current_bit_value <= current_bit_value + in;
                /* Compute bit value using weights. */
                state_bit_calculate:
                begin
                    data <= current_bit_value > 1
                        ? (data | (1 << (state_read - state_read_first_data_bit)))
                        : data;
                    current_bit_value <= 0;
                end
            endcase
        end
    end

endmodule
