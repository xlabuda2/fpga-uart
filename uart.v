/**
 * Application will let you first pick frequency using
 * [KEY1] to pick next frequency and [KEY0] to reset
 * current pick. Frequency is then choosed using [KEY2].
 * After that application will read two bytes from UART
 * pins and send the bigger one after one second delay.
 */
module uart(
    input               CLOCK_50,
    input   wire[3:0]   KEY,
    input   wire[9:0]   SW,
    inout   wire[1:0]   GPIO_0,
    
    output  wire[9:0]   LEDR,
    output  wire[6:0]   HEX0,
    output  wire[6:0]   HEX1,
    output  wire[6:0]   HEX2,
    output  wire[6:0]   HEX3,
    output  wire[6:0]   HEX4,
    output  wire[6:0]   HEX5
);
    
    /* Define prescaler selection state machine. */
    wire[3:0]    prescale_value_pick;
    wire[23:0]   prescale_display_pick;
    
    prescaler_state_machine prescale_module(
        .clock(CLOCK_50),
        .reset(!KEY[0]),
        .next(!KEY[1]),
        .prescale_value(prescale_value_pick),
        .prescale_display(prescale_display_pick)
    );
    
    /* Store value for prescaler. */
    reg[23:0]   baudrate;
    reg[3:0]    prescale_value;
    reg[3:0]    current_prescale; 
    
    /**
     * Display picked frequency depending on the value
     * of an prescaler (default frequency is 115200).
     */
    display display(
        .data(baudrate),
        .digits({HEX5, HEX4, HEX3, HEX2, HEX1, HEX0})
    );
    
    /* UART wires. */
    wire TX;
    wire RX;

    /* Assign UART pins. (RX = left, TX = right) */    
    assign GPIO_0[0]    = TX;    
    assign RX           = GPIO_0[1];
    
    /* Create UART clocks with max baudrate (115.2 kHz). */
    wire uart_transmitter_clock;
    wire uart_receiver_clock;
    
    clock_divider _uart_transmitter_clock(
        .clock(CLOCK_50),
        .prescale_value(prescale_value),
        .out(uart_transmitter_clock)
    );
    
    clock_divider _uart_receiver_clock(
        .clock(CLOCK_50),
        .prescale_value(prescale_value),
        .out(uart_receiver_clock)
    );
    
    /**
     * Desired values for default 115200 bauds.
     * To test if prescaler work, set them to
     * 217, 31 and use baudrate 57600.
     */
    defparam _uart_transmitter_clock.divider = 434;
    defparam _uart_receiver_clock.divider = 62;

    /* Predefine UART registers and wires. */
    reg[7:0]    uart_transmit_data;
    reg         uart_transmit_start;
    wire        uart_transmit_ready;
    
    reg         uart_receiver_clear;
    wire[7:0]   uart_receiver_data;
    wire        uart_receiver_ready;
    
    /* Set UART modules. */
    transmiter _trensmiter(
        .clock(uart_transmitter_clock),
        .start(uart_transmit_start),
        .data(uart_transmit_data),
        .out(TX),
        .ready(uart_transmit_ready)
    );
    
    receiver _receiver(
        .in(RX),
        .clock(uart_receiver_clock),
        .clear_flag(uart_receiver_clear),
        .out(uart_receiver_data),
        .ready(uart_receiver_ready)
    );
    
    /* Define state machine. */
    reg[2:0]    state;
    localparam  state_set_frequency     = 3'd0;
    localparam  state_frequency_pick    = 3'd1;
    localparam  state_wait_for_first    = 3'd2;
    localparam  state_first_received    = 3'd3; 
    localparam  state_wait_for_second   = 3'd4;
    localparam  state_second_received   = 3'd5;
    localparam  state_send_data         = 3'd6;
    localparam  state_data_sent         = 3'd7;
    
    /* Helping registers. */
    reg[7:0]    first_byte;
    reg[7:0]    second_byte;
    
    /* Debug. */
    reg[7:0]    LED;
    
    assign LEDR = LED;
    
    /* So USB can read data. */
    reg[31:0] delay;
    
    localparam second_delay = 115200; 
    
    /* Main loop */ 
    always @(posedge uart_transmitter_clock)
    begin        
        LED <= 1 << state;
       
        case (state)
            state_set_frequency:
            begin
                state           <= (!KEY[2]) ? state_frequency_pick : state_set_frequency;
                baudrate        <= prescale_display_pick;
                prescale_value  <= prescale_value_pick;
            end
            state_frequency_pick:
            begin
                state <= (KEY[2]) ? state_wait_for_first : state_frequency_pick;
            end
            state_wait_for_first:
            begin
                if (uart_receiver_ready == 1)
                begin 
                    state                   <= state_first_received;
                    first_byte              <= uart_receiver_data;
                    uart_receiver_clear     <= 1;
                end
            end
            state_first_received:
            begin
                state                   <= state_wait_for_second;
                uart_receiver_clear     <= 0;
            end
            state_wait_for_second:
            begin
                if (uart_receiver_ready == 1)
                begin 
                    state                   <= state_second_received;
                    second_byte             <= uart_receiver_data;
                    uart_receiver_clear     <= 1;
                end
            end
            state_second_received:
            begin
                state                   <= state_send_data;
                uart_receiver_clear     <= 0;
                delay                   <= second_delay;
            end
            state_send_data:
            begin
                if (delay > 0)
                begin
                    delay <= delay - 1;
                end
                
                if (delay == 0 && uart_transmit_ready == 1)
                begin
                    state               <= state_data_sent; 
                    uart_transmit_data  <= (first_byte > second_byte) ? first_byte : second_byte;
                    uart_transmit_start <= 1;
                end
            end
            state_data_sent:
            begin
                if (uart_transmit_ready == 0)
                begin
                    state               <= state_wait_for_first;
                    uart_transmit_start <= 0;
                end
            end
        endcase
    end

endmodule
