import serial
import serial.tools.list_ports as ports


def send(port, num):
    ser = serial.Serial(port, 115200)

    packet = bytearray([num])

    ser.write(packet)
    ser.close()


def readBlock(port):
    ser = serial.Serial(port, 115200)

    num = ser.read()
    
    ser.close()

    return num


if __name__ == '__main__':
    print(list(map(lambda x: x.device, ports.comports())))

    x = int(input('First number: '))
    send('COM3', x)

    y = int(input('Second number: '))
    send('COM3', y)
    
    print('output: ', int.from_bytes(readBlock('COM3'), byteorder='little'))

